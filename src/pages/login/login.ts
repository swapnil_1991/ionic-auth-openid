import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { OAuthService } from 'angular-oauth2-oidc';
import OktaAuth from '@okta/okta-auth-js';
import { TabsPage } from '../tabs/tabs';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  @ViewChild('email') email: any;
  private username: string;
  private password: string;
  private error: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,private oauthService: OAuthService) {
    oauthService.redirectUri = window.location.origin;
    // oauthService.clientId = '[0oai8go0qePkoiIT90h7]';
    oauthService.clientId = '0oai8lldztsSZRUpY0h7';
    oauthService.scope = 'openid profile email';
    oauthService.issuer = 'https://dev-434118.oktapreview.com/oauth2/default';
  }

  login(): void {
    this.oauthService.createAndSaveNonce().then(nonce => {
      const authClient = new OktaAuth({
        clientId: this.oauthService.clientId,
        redirectUri: this.oauthService.redirectUri,
        url: 'https://dev-434118.oktapreview.com',
        issuer: this.oauthService.issuer
      });
      return authClient.signIn({
        username: this.username,
        password: this.password
      }).then((response) => {
        console.log(response);
        if (response.status === 'SUCCESS') {
          console.log('success');
          return authClient.token.getWithoutPrompt({
            nonce: nonce,
            responseType: ['id_token', 'token'],
            sessionToken: response.sessionToken,
            scopes: ['openid', 'profile', 'email']
          })
            .then((tokens) => {
              console.log(tokens);
              // oauthService.processIdToken doesn't set an access token
              // set it manually so oauthService.authorizationHeader() works
              localStorage.setItem('access_token', tokens[1].accessToken);
              this.oauthService.processIdToken(tokens[0].idToken, tokens[1].accessToken);
              this.navCtrl.push(TabsPage);
            });
        } else {
          throw new Error('We cannot handle the ' + response.status + ' status');
        }
      }).fail((error) => {
        console.error("fail code:",error);
        this.error = error.message;
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    setTimeout(() => {
      this.email.setFocus();
    }, 500);
  }

}
